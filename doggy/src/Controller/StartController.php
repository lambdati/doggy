<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StartController extends Controller
{
    /**
     * @Route("/", name="start")
     */
    public function index(\Swift_Mailer $mailer)
    {
		$message = (new \Swift_Message('Hello Email'))
        ->setFrom('no-reply@sgw-entertainment.de')
        ->setTo('heinrich@sgw-entertainment.de')
        ->setBody(
            $this->renderView(                
                'emails/registration.html.twig',
                array('name' => 'Peter')
            ),
            'text/html'
        );

		//$mailer->send($message);
		
		$base = uniqid().mt_rand(0, mt_getrandmax()).uniqid().mt_rand(0, mt_getrandmax());
		$code = base_convert($base, 16, 36);
		
        return $this->render('start/index.html.twig', [
            'controller_name' => 'StartController',
			'code' => $code
        ]);
    }
}
