<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swift_Attachment;

class UserController extends Controller
{
    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', ['controller_name' => $this->generateUrl('start', array('username' => 'hans'))]);
    }
	
	 /**
     * @Route("/user/{username}", name="showuser")
     */
    public function user($username, \Swift_Mailer $mailer)
    {
		$title = "Hallo";
		
		$message = (new \Swift_Message('Hello Email'))
        ->setFrom('no-reply@sgw-entertainment.de')
        ->setTo('heinrich@sgw-entertainment.de')
		->setSubject($username.', eine Nachricht wartet auf dich')
		->attach(Swift_Attachment::fromPath('http://www.orimi.com/pdf-test.pdf'))
        ->setBody(
            $this->renderView(                
                'emails/newsletter.html.twig',
                array('username' => $username, 'title' => $title)
            ),
            'text/html'
        );

		$mailer->send($message);
		
        return $this->render('user/user.html.twig', ['controller_name' => 'UserController', 'username' => $username]);
    }
}
