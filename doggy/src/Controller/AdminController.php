<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    /**
     * @Route("/", name="admin", host="admin.{domain}")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', ['controller_name' => 'AdminController']);
    }
}
